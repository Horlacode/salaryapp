<?php
    session_start();
    require_once("includes/dbconnection.php");  
    // Total Salary
    $query3 = "SELECT SUM(salary) AS Totalsalary FROM salary_tab";
    $result3 = mysqli_query($conn, $query3);
    $total = 0;
    $row = mysqli_fetch_array($result3);
    $total =$row['Totalsalary'];

    // Total Adance Salary
    $query4= "SELECT SUM(advance) AS Totaladvance FROM salary_tab";
    $result4 = mysqli_query($conn, $query4);
    $total2 = 0;
    $row2 = mysqli_fetch_array($result4);
    $total2 =$row2['Totaladvance'];

        // Total Expected Salary
    $query5= "SELECT SUM(expected) AS TotalExpected FROM salary_tab";
    $result5 = mysqli_query($conn, $query5);
    $total3 = 0;
    $row3 = mysqli_fetch_array($result5);
    $total3 =$row3['TotalExpected'];
       // Total Bonus 
    $query9= "SELECT SUM(bonus) AS TotalBonus FROM salary_tab";
    $result9 = mysqli_query($conn, $query9);
    $total9 = 0;
    $row4 = mysqli_fetch_array($result9);
    $total9 =$row4['TotalBonus'];
          // Total deduct
    $query10= "SELECT SUM(deduct) AS TotalDeduct FROM salary_tab";
    $result10 = mysqli_query($conn, $query10);
    $total10 = 0;
    $row5 = mysqli_fetch_array($result10);
    $total10 =$row5['TotalDeduct'];


 ?>

<?php include("includes/navheader.php"); ?>
 <!----Pages Designs will be inserted here -->
   <div class="container pt-4 mt-5">
        <div class="row">  
            <div class="col-sm-12">
               <?php if(isset($_SESSION['message'])){
                  ?><div class="alert alert-info alert-dissimible">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <?php echo $_SESSION['message']; ?>
                      <?php unset($_SESSION['message']); ?>
                    </div> 
                <?php } ?>
                <div class="alert alert-info">
                    <h4 class=""><b>Admin PageAccess</b></h4>
                </div>
                <div class="table-responsive-sm">
                  <table class="table table-striped">
                    <p class="ml-2"><b>All Staffs Member Table</b></p>
                    <thead class="thead-dark">
                      <tr>
                        <th>ID</th>
                        <th>STAFFID</th>
                        <th>FirstName</th>
                        <th>LastName</th>
                        <th>Email</th>
                        <th>Salary</th>
                        <th>Advance Payment</th>
                        <th>Bonus</th>
                        <th>Deduct</th>
                        <th>Expected Payment</th>
                        <th>Action</th>
                        <th>Pay</th>
                        <th>Status</th>
                        <th>Print</th>
                      </tr>
                    </thead>
                    <tbody>
                       <?php 
                            $mm = date('m');
                            $query = "SELECT * FROM `salary_tab`";
                            $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
                            while($row = mysqli_fetch_array($result)){
                                ?>
                              <tr class="text-dark">
                                  <td><?php echo $row['id']; ?></td>
                                  <td><?php echo $row['sn']; ?></td>
                                  <td><?php echo $row['firstname']; ?></td>
                                  <td><?php echo $row['lastname']; ?></td>
                                  <td><?php echo $row['email']; ?></td>
                                  <td>N<?php echo $row['salary']; ?></td>
                                  <td>N<?php echo $row['advance']; ?></td>
                                  <td>N<?php echo $row['bonus']; ?></td>
                                  <td>N<?php echo $row['deduct']; ?></td>
                                  <td>N<?php echo $row['expected']; ?></td>
                                  <td><form method="post" action="staff_dash.php">
                                   <input type="hidden" name="id" value="<?php echo($row['id']); ?>" />
                                   <button class='btn btn-sm btn-success' type="submit">Profile</button>
                                 </form></td>
                                  <td><?php echo "<a class='btn btn-sm btn-danger' href='pay.php?id=".$row['id']."'>Pay</a>" ?>
                                 </td>
                                 <td><button class="btn btn-sm btn-info"><?php echo $row['status'] ?></button></td> 
                                 <td><?php echo "<a class='btn btn-sm btn-secondary' href='receipt.php?id=".$row['id']."'>Print</a>" ?>
                                 </td> 
                               </tr>  
                            <?php } ?>
                    </tbody>
                  </table>
                  <div class="">
                    <p>Total Salary is :  <b><?php echo $total ?></b>
                       Total Advance Salary is :  <b><?php echo $total2 ?></b>
                       Total Expected Salary  is : <b><?php echo $total3 ?></b>
                       Total Bonus  is : <b><?php echo $total9 ?></b>
                       Total Deduct  is : <b><?php echo $total10 ?></b></p>
                    <a href="sign_up.php" class="btn btn block btn-info">Add Staff</a> 
                  </div>
                      
          </div> 
        </div>
   </div>
 </body>
</html>
<?php include("includes/scripts.php")?>

  <!--<tr><td colspan="10" class="text-center text-white bg-secondary">No Course Added Yet!!</td></tr>-->