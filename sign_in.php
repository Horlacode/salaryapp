<?php
      session_start();
      require_once("includes/dbconnection.php");

      $errors = array();
      if(isset($_POST['submit'])) {         
        $username = mysqli_real_escape_string($conn, $_POST['username']);
        $password = mysqli_real_escape_string($conn, $_POST['password']);
      
      if((empty($username)) && (empty($password))){
        $errors[] = "Username and Password cant be emtpy";
      }
      if(empty($errors)){
        $query = "SELECT * FROM `salary_tab` WHERE username='$username'";
        $result= mysqli_query($conn, $query) or die(mysql_error($conn));
          if(mysqli_num_rows($result) <=0 ){
              $errors[]= 'User not found';
            }else{ 
            $row=mysqli_fetch_assoc($result);
              if($row['password'] != ($password)){
                $errors[]= 'Invalid Password';
              }else{
                  $_SESSION['id'] = $row['id'];
                  $_SESSION['username'] = $row['username'];
                  header("location: admin_dash.php");
              }
          }
        }
      }          
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <title>Login</title>
    </head>
    <body>  
        <!---=====================Register Form is right here=====--->
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-3 mt-5">
                    <!--displaying Error mssgs as well -->
                    <?php if(count($errors)> 0) { ?>
                       <div class="alert alert-danger alert-dissimible">
                          <?php foreach($errors as $error){ ?>
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <?php echo $error .'<br>';?>
                          <?php }; ?>
                        </div>
                    <?php } ;?> 
                    <div class="card shadow-lg">
                        <div class="card-body">
                            <form method="POST" action="sign_in.php">
                                <input type="hidden" name="submit" value="anything">
                                  <h4 class="text-center"><b>Account Login</b>
                                   </h4><hr>
                                   <!--Show errors---> 
                                  <div class="form-group">
                                    <label class="text-light-white fs-14">Username*</label>
                                    <input type="text" name="username"
                                           class="form-control form-control-submit"
                                           placeholder="username">
                                  </div>
                                  <div class="form-group">
                                    <label class="text-light-white fs-14">Password*</label>
                                    <input type="password" id="password-field" name="password"
                                           class="form-control form-control-submit"
                                           placeholder="password">
                                  </div>
                                  <div class="form-group text-center">
                                      <button class="btn btn-block btn-secondary" name="signup" type="submit">Sign Up</button>
                                  </div>
                            </form>
                            <div class="text-center text-muted pt-3">
                                <p><a href="#">Terms & conditions</a>|<a href="#">Privacy & Policy</a></p>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>  
        </div>
        <?php include("includes/scripts.php"); ?>

    </body>
</html>
 
