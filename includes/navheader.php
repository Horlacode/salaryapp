<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <title>Dashboard</title>
        <style>
            .navbar{
                background-color: #212f42 !important;
            }
            .footer{
                background: #212f42 !important;
                margin-top: 330px;
                position: relative;
            }
        </style>

    </head>

    <body>
        <!--=======================Start of Navbar=====================-->
        <nav class="navbar navbar-expand-md bg-light navbar-light fixed-top">
            <div class="container">
                <a href="admin_dash.php"><button class="navbar-brand btn btn-success"
                   data-toggle="modal" data-target="#exampleModalLong">
                    <span class="text-white"><b>SalaryAPP</b></span></button></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
                    <ul class="navbar-nav ">
                        <li><a class="btn btn-danger ml-2 mt-1" type="submit" name="submit" href="logout.php">
                        Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        