<?php
   session_start();
    //print_r($_POST);
    require_once("includes/dbconnection.php");
   
    if(isset($_POST['signup'])) {
      $sn = rand(1111,9999);       
      $firstname = mysqli_real_escape_string($conn, $_POST['firstname']);
      $lastname = mysqli_real_escape_string($conn, $_POST['lastname']);
      $email = mysqli_real_escape_string($conn, $_POST['email']);
      $salary = mysqli_real_escape_string($conn, $_POST['salary_amt']);
      $position = mysqli_real_escape_string($conn, $_POST['position']);
      $photo =strtolower($_FILES['photo']['name']);
      
      $file_ext = substr($photo, strpos($photo, '.'));
      $path = 'uploads/images'.$file_ext;
      $success = move_uploaded_file($_FILES['photo']['tmp_name'], $path); 
        # empty validation 
        if(!$success){
          $_SESSION['errmssg'] = "File not saved";
        }
        if((empty($firstname)) || (empty($lastname)) || (empty($email)) || (empty($salary))){
          $_SESSION['errmssg'] = "Fields cant be empty";
        }

       if((!empty($firstname)) && (!empty($lastname)) && (!empty($email)) && (!empty($salary))){
        $query = "INSERT INTO `salary_tab` (`sn`,`firstname`, `lastname`, `email`, `salary`, `expected`, `position`, 
        `profile_pics`) VALUES ('".$sn."',
        '".$firstname."', '".$lastname."', '".$email."', '".$salary."', '".$salary."', '".$position."', '".$photo."')";
        $result = mysqli_query($conn, $query);
        $_SESSION['success'] = "Staff Added Successfully";
        header("location: admin_dash.php"); 
        exit;
      }  
    }      
?>     
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.css">
        <title>Staff registration page</title>
    </head>
    <body>  
        <!---=====================Register Form is right here=====--->
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-3 mt-5">
                    <div class="card shadow-lg">
                        <div class="card-body">
                            <form method="POST" action="sign_up.php" enctype="multipart/formdata">
                                <input type="hidden" name="submit" value="anything">
                                  <h4 class="text-center"><b>ADD STAFF</b>
                                   </h4><hr>
                                   <!--Show errors--->
                                   <?php if(isset($_SESSION['errmssg'])){
                                      ?><div class="alert alert-info alert-dissimible">
                                          <button type="button" class="close" data-dismiss="alert">&times;</button>
                                          <?php echo $_SESSION['errmssg']; ?>
                                          <?php unset($_SESSION['errmssg']); ?>
                                        </div> 
                                    <?php } ?> 
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="text-light-white">First Name</label>
                                        <input type="text" name="firstname"
                                               class="form-control"
                                               placeholder="first name">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="text-light-white">Last Name</label>
                                        <input type="text" name="lastname"
                                               class="form-control"
                                               placeholder="last name">
                                      </div>
                                    </div>
                                  </div> 
                                  <div class="form-group">
                                    <label class="text-light-white">Email Adress*</label>
                                    <input type="email" name="email"
                                           class="form-control"
                                           placeholder="email address">
                                  </div>
                                  <div class="form-group">
                                    <label class="text-light-white">Salary*</label>
                                    <input type="number" name="salary_amt"
                                           class="form-control"
                                           placeholder="monthly salary">
                                  </div>
                                   <div class="form-group">
                                      <label class="text-light-white">Position Held*</label>
                                      <input type="text" name="position"
                                             class="form-control"
                                             placeholder="position">
                                    </div>
                                  <div class="form-group">
                                      <label class="text-light-white">Profile Pics*</label>
                                      <input type="file" name="photo"
                                             class="form-control">
  
                                    </div> 
                                  <div class="form-group text-center">
                                      <button class="btn btn-block btn-secondary" name="signup" type="submit">
                                      ADD STAFF</button>
                                  </div>
                            </form>
                            <div class="text-center text-muted pt-3">
                                <p><a href="#">Terms & conditions</a>|<a href="#">Privacy & Policy</a></p>
                            </div>
                     <!---=====================End of Register Form=====--->
                        </div>
                    </div>    
                </div>
            </div>  
        </div>
        <?php include("includes/scripts.php"); ?>

    </body>
</html>


    

   <!--  if((empty($firstname)) || (empty($lastname)) || (empty($email)) || (empty($salary))){
        $_SESSION['errmssg'] = "Fields cant be empty";
      }
      # length, and regex checks...
      if($password != $password2){
        $_SESSION['errmssg'] = "Password is not a Match";
      }
      if((!empty($username)) && (!empty($password)) && (!empty($role)) && (!empty($salary))){
        $hashedpass = md5($password);
        // first checked if the username already existed in the Db
        $query1 = "SELECT * FROM `salary_tb` WHERE username='$username'";
        $result1 = mysqli_query($conn, $query1) or die(mysql_error($conn));
        if(mysqli_num_rows($result1) == 1){
          $_SESSION['errmssg'] = "Username already exists";
          header("location: sign_up.php");
          exit;
        }
        $query = "INSERT INTO `salary_tb` (`firstname`, `lastname`, `email`, `salary`, `designation`,
        `username`, `password`) VALUES ('".$firstname."', '".$lastname."', '".$email."',
        '".$salary."', '".$role."', '".$username."', '".$hashedpass."')";
        //echo $query;
        $result = mysqli_query($conn, $query);
        $_SESSION['success'] = "Registration Successful";
        header("location: sign_in.php"); 
        exit;
      } 
       // A function to validate the pics coming in ...

    // function fileExt($photo){
    //   $ext = strtolower(substr($photo, strpos($photo, '.'), strlen($photo)-1));
    //   if($ext=='.jpg' OR $ext=='.jpeg' OR $txt=='.png'){$res =TRUE;}else{$res=FALSE;}
    //   return $res;
   // $success=move_uploaded_file($_FILES['photo']['tmp_name'], uploads.$photo);
    // } 
    }  -->