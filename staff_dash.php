<?php 
   session_start();
   include("includes/dbconnection.php");

   //$id = $_SESSION['id'];
     if(isset($_REQUEST['id'])){
        $id = $_REQUEST['id'];

        $query = "SELECT * FROM salary_tab WHERE id='$id' ";
         
        $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
        $row = mysqli_fetch_array($result);
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $email = $row['email']; 
        $salary_payment = $row['salary'];
        $requested = $row['advance'];
        $expected_payment = $row['expected'];
        $position  = $row['position'];
        $bonus = $row['bonus'];
        $deduct = $row['deduct'];
        
        //setting the day, month and year variables
          $dd = date('d');
          $mm = date('m');
          $yy = date('y');
        //submission and data validation ..
        if (isset($_POST['submit'])){
            $advanc= $_POST['salary_amt'];
            $bonus_money = $_POST['bonus'];
            $deduct_money = $_POST['deduct']; 

            $total_advance = $requested + $advanc; // this add this advance money reqeusted for in the database 
            $total_bonus = $bonus + $bonus_money;
            $total_deducts = $deduct + $deduct_money;
            $total_adv_deduct = $total_advance + $total_deducts;
            $total_sal_bonus = $total_bonus + $salary_payment;
            if($total_advance > $salary_payment){
              $errmssg = "Insufficienct Funds!! Request lesser MOU";
            }
            if($total_adv_deduct > $total_sal_bonus){
              $errmssg = "Insufficient Funds!!";
            }
            else{
              $expected_amount = $salary_payment + $total_bonus - $total_advance - $total_deducts ;

              $query2 = "UPDATE salary_tab SET advance='$total_advance', bonus='$total_bonus', 
              deduct='$total_deducts', expected='$expected_amount', 
              dd='$dd', mm='$mm', yy='$yy' WHERE id = '$id'" ;
              $result2 = mysqli_query($conn, $query2) or die(mysqli_error($conn));
              header("location: admin_dash.php");
              $_SESSION['message'] = "Requests Granted";
            }
          }
        }
?>
<?php include("includes/navheader.php"); ?>
   <div class="container">
        <div class="row">
            <div class="col-md-6 offset-3 mt-5">
                <div class="card shadow-lg">
                    <div class="card-body">
                        <form method="POST" action="<?php $_SERVER['REQUEST_URI']?>">
                            <input type="hidden" name="submit" value="anything">
                             <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                              <h4 class="text-center">
                                <img src="img/norms.jpg" style="width: 50px; height: 50px;" class="rounded-circle mr-2">
                                <b>STAFF PROFILE</b>
                               </h4><hr>
                               <!--Show errors--->
                               <?php if(isset($errmssg)){
                                  ?><div class="alert alert-info alert-dissimible">
                                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                                      <?php echo $errmssg; ?>
                                    </div> 
                                <?php } ?> 
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="text-light-white">First Name</label>
                                    <input type="text" name="firstname" value="<?php echo $firstname ?>" 
                                           class="form-control"
                                           placeholder="first name">
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="text-light-white">Last Name</label>
                                    <input type="text" name="lastname" value="<?php echo $lastname ?>" 
                                           class="form-control"
                                           placeholder="last name">
                                  </div>
                                </div>
                              </div> 
                              <div class="form-group">
                                <label class="text-light-white">Email Adress*</label>
                                <input type="email" name="email" value="<?php echo $email ?>" 
                                       class="form-control"
                                       placeholder="email address">
                              </div>
                              <div class="form-group">
                                <label class="text-light-white">Salary*</label>
                                <input type="number" name="salary_amt" value="<?php echo $salary_payment ?>" 
                                       class="form-control"
                                       placeholder="monthly salary">
                              </div>
                              <div class="form-group">
                                    <label class="text-light-white">Position</label>
                                    <input type="text" name="position" value="<?php echo $position ?>"
                                           class="form-control"
                                           placeholder="first name">
                                  </div>
                              <div class="form-group">
                                <label class="text-light-white">Present Salary*</label>
                                <input type="text" name="salary_amted" value="<?php echo $expected_payment ?>" 
                                       class="form-control">
                                      
                              </div>
                              <div class="form-group">
                                <label class="text-light-white">Bonus*</label>
                                <input type="text" name="bonus" value="<?php echo $bonus ?>" 
                                       class="form-control">
                                     
                              </div>
                              <div class="form-group">
                                <label class="text-light-white">Deduct*</label>
                                <input type="text" name="deduct" value="<?php echo $deduct ?>" 
                                       class="form-control">
                                   
                              </div>
                              <div class="form-group">
                                <label class="text-light-white">Request Advance Salary*</label>
                                <input type="number" name="salary_amt" value="<?php echo $requested ?>" 
                                       class="form-control"
                                       placeholder="monthly salary">
                              </div>
                              <div class="form-group text-center">
                                  <button class="btn btn-block btn-secondary" name="submit" type="submit">
                                  Update</button>
                              </div>
                        </form>
                        <div class="text-center text-muted pt-3">
                            <p><a href="#">Terms & conditions</a>|<a href="#">Privacy & Policy</a></p>
                        </div>
                 <!---=====================End of Register Form=====--->
                    </div>
                </div>    
            </div>
        </div>  
    </div>
 </body>
</html> 
<?php include("includes/scripts.php")?>
 <!--href='profile_update.php?id=".$row['id']."'-->}
